use base64::{engine::general_purpose, Engine};
use layout::{
    backends::svg::SVGWriter,
    gv::{DotParser, GraphBuilder},
};
use leptos::*;
use leptos_router::*;
use regex::Regex;

use wasm_bindgen::prelude::*;

use crate::svgpanzoom::setSvgPanZoom;

#[wasm_bindgen]
extern "C" {
    // Use `js_namespace` here to bind `console.log(..)` instead of just
    // `log(..)`
    #[wasm_bindgen(js_namespace = console)]
    fn log(s: &str);
}

const INITIAL_GRAPH: &str = r#"digraph G {
    subgraph cluster_0 {
        a0 -> a1 -> a2 -> a3;
    }
    
    subgraph cluster_1 {
        node [style=filled];
        b0 -> b1 -> b2 -> b3;
    }
    start -> a0;
    start -> b0;
    a1 -> b3;
    b2 -> a3;
    a3 -> a0;
    a3 -> end;
    b3 -> end;
}"#;

const FAILED_TO_COMPILE: &str =
    "<svg width=\"100\" height=\"50\" xmlns=\"http://www.w3.org/2000/svg\"><text x=\"50%\" y=\"50%\" fill=\"red\" dominant-baseline=\"middle\" text-anchor=\"middle\">Failed to compile!</text></svg>";

fn rebuild_svg(dot: &String) -> Result<String, ()> {
    let mut parser = DotParser::new(dot);
    let result = parser.process();
    if let Ok(g) = result {
        let mut gb = GraphBuilder::new();
        gb.visit_graph(&g);
        let mut vg = gb.get();
        let mut svg = SVGWriter::new();
        vg.do_it(false, false, false, &mut svg);
        let mut svg = svg.finalize();
        for _ in 0.."<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>".len() {
            svg.remove(0);
        }
        let re1 = Regex::new("<svg[^>]*>").unwrap();
        let res1 = re1.replace(&svg, "<svg id=\"controlsvg\">");
        let re2 = Regex::new("</svg>").unwrap();
        let res2 = re2.replace(&res1, "</svg>");
        Ok(res2.to_string())
    } else {
        Err(())
    }
}

#[component]
pub(crate) fn App() -> impl IntoView {
    // Setup signals and the update function
    let (b64text, set_b64text) = create_query_signal::<String>("graph");
    let (text, set_text) = create_signal(String::new());
    let (svg, svg_writer) = create_signal(String::new());
    let update_text = move |t: String| {
        let svg = rebuild_svg(&t);
        let base64str = general_purpose::URL_SAFE.encode(&t);
        set_text.set(t);
        set_b64text.set(Some(base64str));
        if let Ok(svg_text) = svg {
            svg_writer.set(svg_text);
        } else {
            svg_writer.set(FAILED_TO_COMPILE.to_string());
        }
    };

    // Set the initial page state, defaulting to an example graph if no graph is given
    untrack(move || {
        if let Some(base64str) = b64text.get() {
            if let Ok(vec) = general_purpose::URL_SAFE.decode(&base64str) {
                if let Ok(str) = String::from_utf8(vec) {
                    update_text(str);
                    return;
                }
            }
        }
        update_text(INITIAL_GRAPH.to_string());
    });

    let refresh_panzoom = create_resource(
        move || svg.get(),
        |_| async move { setSvgPanZoom("#controlsvg") },
    );

    view! {
        <div class="flex h-full">
            <div class="flex-1 box-border h-auto">
                <textarea
                    class="h-full w-full box-border resize-none overflow-wrap-none font-mono whitespace-nowrap caret-black text-sm outline-none p-1"
                    inner_html=move || { text.get() }
                    on:input=move |ev| { update_text(event_target_value(&ev)) }
                >
                </textarea>
            </div>
            <div class="flex-1 box-border h-auto"
                inner_html=move || { svg.get() }
            >
            </div>

        </div>
    }
}
