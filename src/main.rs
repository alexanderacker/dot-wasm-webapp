use leptos::*;
use leptos_router::*;

use crate::app::App;

mod app;
mod svgpanzoom;

fn main() {
    mount_to_body(|| {
        view! {
            <Router>
                <Routes>
                    <Route path="" view=App />
                </Routes>
            </Router>
        }
    });
}
